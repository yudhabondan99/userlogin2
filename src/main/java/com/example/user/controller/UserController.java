package com.example.user.controller;

import com.example.user.model.BaseResponse;
import com.example.user.model.wrapper.UserEnt;
import com.example.user.service.UserService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
public class UserController {
    Gson g = new Gson();
    String result = null;
    @Autowired
    private UserService userService;

    @PostMapping("/saveUser")
    public BaseResponse<UserEnt> save(@RequestBody UserEnt user) throws Exception {
        BaseResponse<UserEnt> saveUser = userService.save(user);
        return saveUser;
    }

}
