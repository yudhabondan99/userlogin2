package com.example.user.service;

import com.example.user.model.BaseResponse;
import com.example.user.model.wrapper.UserEnt;
import com.example.user.repo.RoleRepo;
import com.example.user.repo.UserRepo;
import com.example.user.repo.UserRoleRepo;
import com.example.user.util.TripleDesEncDec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;

@Service
public class UserService {
    @Autowired
    private UserRepo userRepo;

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private UserRoleRepo userRoleRepo;

    public BaseResponse<UserEnt> save(UserEnt user) throws Exception {
        String idRole="1",idUser="11111";
        TripleDesEncDec tripED= new TripleDesEncDec();
        BaseResponse<UserEnt> response = new BaseResponse<UserEnt>();
        String id = user.getId();
        String nama = user.getNama();

        String dateCreated = user.getDate_created();

        String pass = user.getPassword();
        byte[] codedtext = encrypt(pass);
        pass = Base64.getEncoder().encodeToString(codedtext);

        try {
//            userRoleRepo.findBySomeField(idRole,idUser);

            userRepo.saveData(id,nama,dateCreated,pass);
            response.setStatus(200);
            response.setMessage("Success");
        } catch (Exception ex) {
            response.setStatus(500);
            response.setMessage("INTERNAL SERVER ERROR");
        }
        return response;
    }



    public byte[] encrypt(String message) throws Exception {
        final MessageDigest md = MessageDigest.getInstance("md5");
        final byte[] digestOfPassword = md.digest("HG58YZ3CR9"
                .getBytes("utf-8"));
        final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
        for (int j = 0, k = 16; j < 8;) {
            keyBytes[k++] = keyBytes[j++];
        }

        final SecretKey key = new SecretKeySpec(keyBytes, "DESede");
        final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
        final Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key, iv);

        final byte[] plainTextBytes = message.getBytes("utf-8");
        final byte[] cipherText = cipher.doFinal(plainTextBytes);
        // final String encodedCipherText = new sun.misc.BASE64Encoder()
        // .encode(cipherText);

        return cipherText;
    }

    public String decrypt(byte[] message) throws Exception {
        final MessageDigest md = MessageDigest.getInstance("md5");
        final byte[] digestOfPassword = md.digest("HG58YZ3CR9"
                .getBytes("utf-8"));
        final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
        for (int j = 0, k = 16; j < 8;) {
            keyBytes[k++] = keyBytes[j++];
        }

        final SecretKey key = new SecretKeySpec(keyBytes, "DESede");
        final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
        final Cipher decipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        decipher.init(Cipher.DECRYPT_MODE, key, iv);

        // final byte[] encData = new
        // sun.misc.BASE64Decoder().decodeBuffer(message);
        final byte[] plainText = decipher.doFinal(message);

        return new String(plainText, "UTF-8");
    }

}
