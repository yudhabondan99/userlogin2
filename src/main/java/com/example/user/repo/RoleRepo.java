package com.example.user.repo;

import com.example.user.model.wrapper.RoleEnt;
import com.example.user.model.wrapper.UserEnt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<RoleEnt,String> {

}
