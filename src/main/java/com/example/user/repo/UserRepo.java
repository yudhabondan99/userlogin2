package com.example.user.repo;

import com.example.user.model.wrapper.UserEnt;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;

public interface UserRepo extends JpaRepository<UserEnt,String> {
    @Modifying
    @Query(value = "INSERT INTO public.user(id, nama, date_created, password) VALUES (:someValueId,:someValueNama,:someValueDate,:someValuePassword)", nativeQuery = true)
    @Transactional
    void saveData(@Param("someValueId") String id,
                  @Param("someValueNama") String nama, @Param("someValueDate") String date_created, @Param("someValuePassword") String password);

}
