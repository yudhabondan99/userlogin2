package com.example.user.repo;

import com.example.user.model.wrapper.UserEnt;
import com.example.user.model.wrapper.UserRoleEnt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRoleRepo extends JpaRepository<UserRoleEnt,String> {

    @Query(value = "SELECT * FROM public.user WHERE id_user = :someValueIdU and id_role = :someValueIdR", nativeQuery = true)
    List<UserRoleEnt> findBySomeField(@Param("someValueIdU") String id_user,@Param("someValueIdR") String id_role);
}
