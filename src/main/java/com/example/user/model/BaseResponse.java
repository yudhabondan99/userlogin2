package com.example.user.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class BaseResponse<T> {
    private int status;
    private String message;
    private List<T> data;

    // Constructors, getters, and setters

    public BaseResponse(int status, String message, List<T> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public BaseResponse() {
    }

}
