package com.example.user.model.wrapper;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@Table(name="user")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserEnt {

    @Id
    @Column(name ="id")
    private String id;

    @Column(name ="nama")
    private String nama;

    @Column(name ="date_created")
    private String date_created;

    @Column(name ="password")
    private String password;



}
