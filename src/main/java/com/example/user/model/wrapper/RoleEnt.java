package com.example.user.model.wrapper;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@Table(name="role")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleEnt {

    @Id
    @Column(name ="id")
    private String id;

    @Column(name ="nama_role")
    private String namaRole;
}
