package com.example.user.model.wrapper;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Entity
@Table(name="user_role")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRoleEnt {
    @Id
    @Column(name ="id_user")
    private String idRole;

    @Column(name ="nama_role")
    private String namaRole;

    @Column(name ="id_role")
    private String id_role;

}
